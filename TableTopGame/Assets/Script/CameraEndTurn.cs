﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraEndTurn : MonoBehaviour {
    public Transform[] view;
    

    public float transitionSpeed;

    public Button button1;
    public Button button2;
    public Button button3;

    public GameObject back;
    public GameObject topview;
    public GameObject endturn;

    [HideInInspector]
    public int i = 0;
    [HideInInspector]
    public bool isTop;

    Transform currentView;
	// Use this for initialization
	void Start () {
        Button btn1 = button1.GetComponent<Button>();
        Button btn2 = button2.GetComponent<Button>();
        Button btn3 = button3.GetComponent<Button>();

        button1.onClick.AddListener(TaskOnClick);
        button2.onClick.AddListener(TopView);
        button3.onClick.AddListener(Back);

        GameObject.FindGameObjectWithTag("back").SetActive(false);
        

        isTop = false;
    }
    void TaskOnClick()
    {
        i = i + 1;
        if (i > 3)
        {
            i = 0;
        }
        
    }
    void TopView()
    {
        i = i + 4;
    }
    void Back()
    {
        i = i - 4;
    }


  
    // Update is called once per frame
    void LateUpdate () {
            if(i == 4 || i == 5 || i == 6 || i == 7)
        {
            currentView = view[4];
            topview.gameObject.SetActive(false);
            endturn.gameObject.SetActive(false);
            back.gameObject.SetActive(true);
            isTop = true;
        }
        else
        {
            topview.gameObject.SetActive(true);
            endturn.gameObject.SetActive(true);
            back.gameObject.SetActive(false);
            currentView = view[i];
            isTop = false;

        }
            
            transform.position = Vector3.Lerp(transform.position, currentView.position, Time.deltaTime * transitionSpeed);
        
       
        
    }
    
}
