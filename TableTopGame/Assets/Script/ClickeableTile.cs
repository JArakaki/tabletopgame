﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickeableTile : MonoBehaviour {
    public int tileX;
    public int tileY;

    public TileMap map;

    private void OnMouseDown()
    {
        Debug.Log("Click");
        Debug.Log("Tile x:" + tileX +"Tile z:" + tileY);
        map.GeneratePathTo(tileX, tileY);
    }
    
}
