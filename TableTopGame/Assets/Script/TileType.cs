﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TileType
{
    public string name;
    public GameObject tilevisualPrefab;

    public float movementcost = 1f;

    public bool isWalkable = true;
}
