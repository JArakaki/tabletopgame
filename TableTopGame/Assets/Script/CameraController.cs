﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public Transform target;
    
    
    public float speedH = 2.0f;
    public float speedV = 2.0f;
    
    private bool looking = false;

    
    

    [Range(0f, 65f)]
    public float freeLookSensitivity = 3f;

    [Range(0f, 65f)]
    public float speed;

    private void Start()
    {
        
      transform.LookAt(target);
    }

    

    void LateUpdate()
        
    {
        
        if (looking)
        {
            float newRotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * freeLookSensitivity;
            float newRotationY = transform.localEulerAngles.x - Input.GetAxis("Mouse Y") * freeLookSensitivity;
            transform.localEulerAngles = new Vector3(newRotationY, newRotationX, 0f);

        }
        else
        {
            if (gameObject.GetComponent<CameraEndTurn>().isTop == true)
            {
                if(gameObject.GetComponent<CameraEndTurn>().i == 4)
                {
                    transform.rotation = Quaternion.Euler(90, 0, 0);
                }
                if (gameObject.GetComponent<CameraEndTurn>().i == 5)
                {
                    transform.rotation = Quaternion.Euler(90, 0, -90);
                }
                if (gameObject.GetComponent<CameraEndTurn>().i == 6)
                {
                    transform.rotation = Quaternion.Euler(90, 0, 180);
                }
                if (gameObject.GetComponent<CameraEndTurn>().i == 7)
                {
                    transform.rotation = Quaternion.Euler(90, 0, 90);
                }

            }
            else
            {
                transform.LookAt(target);
            }

        }
     
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            StartLooking();
        }
        else if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            StopLooking();
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position = transform.position + (-transform.right * speed* Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            transform.position = transform.position + (transform.right * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            transform.position = transform.position + (transform.forward * speed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            transform.position = transform.position + (-transform.forward * speed * Time.deltaTime);
        }
        
    }
   
    

    public void StartLooking()
        {
            looking = true;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        gameObject.GetComponent<CameraEndTurn>().enabled = false;
    }

    void OnDisable()
    {
        StopLooking();
    }

    public void StopLooking()
    {
        looking = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        gameObject.GetComponent<CameraEndTurn>().enabled = true;
        
    }
       
        
      
            
        
    
}
