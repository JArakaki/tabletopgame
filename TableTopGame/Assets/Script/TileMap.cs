﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TileMap : MonoBehaviour
{
    public GameObject unit;

    public TileType[] tileTypes;

    int[,] tiles;
    Node[,] graph;


    int mapSizeX = 13;
    int mapSizeY = 13;

    private void Start()
    {
        unit.GetComponent<Unit>().tileX = (int)unit.transform.position.x;
        unit.GetComponent<Unit>().tileY = (int)unit.transform.position.z;
        unit.GetComponent<Unit>().map = this;

        GenerateMapData();
        GeneratePathfindingGraph();
        GenerateMap();
    }
    void GenerateMapData()
    {
        tiles = new int[mapSizeX, mapSizeY];

        int x, y;

        //GroundType
        for (x = 0; x < mapSizeX; x++)
        {
            for (y = 0; y < mapSizeY; y++)
            {
                tiles[x, y] = 0;
            }
        }
        //Players(1-4)
        tiles[0, 6] = 4;
        tiles[12, 6] = 6;
        tiles[6, 0] = 3;
        tiles[6, 12] = 5;
        tiles[6, 6] = 7;

        //Floor
        tiles[6, 1] = 1;
        tiles[6, 2] = 1;
        tiles[6, 3] = 1;

        tiles[5, 0] = 1;
        tiles[4, 0] = 1;
        tiles[3, 0] = 1;

        tiles[7, 0] = 1;
        tiles[8, 0] = 1;
        tiles[9, 0] = 1;

        tiles[9, 1] = 1;
        tiles[9, 2] = 1;
        tiles[9, 3] = 1;

        tiles[7, 2] = 1;
        tiles[8, 2] = 1;
        tiles[9, 2] = 1;

    }

    public float CostToEnterTile(int sourceX, int sourceY, int targetX, int targetY) 
    {
        TileType tt = tileTypes[tiles[targetX, targetY]];
        if (UnitCanEnterTile(targetX,targetY) == false)
            return Mathf.Infinity;
        float cost = tt.movementcost;
        if (sourceX != targetX && sourceY != targetY)
        {
            cost += 0.01f;
            return tt.movementcost;
        }
        return cost;
    }


    void GenerateMap()
    {
        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                TileType tt = tileTypes[tiles[x, y]];
                GameObject go = (GameObject)Instantiate(tt.tilevisualPrefab, new Vector3(x, 0, y), Quaternion.identity);
                ClickeableTile ct = go.GetComponent<ClickeableTile>();
                ct.tileX = x;
                ct.tileY = y;
                ct.map = this;
            }
        }
    }


    public class Node
    {
        public List<Node> edges;
        public int x;
        public int y;

        public Node()
        {
            edges = new List<Node>();
        }

        public float DistanceTo(Node n)
        {
            return Vector3.Distance(new Vector3(x, 0, y), new Vector3(n.x, 0, n.y));
        }
    }





void GeneratePathfindingGraph()
    {
        //Inicia el array
        graph = new Node[mapSizeX, mapSizeY];

        //Inicializa un nodo por cada posicion en el array
        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                graph[x, y] = new Node();
                graph[x, y].x = x;
                graph[x, y].y = y;
            }
        }

        //Calcular edges
        for (int x = 0; x < mapSizeX; x++)
        {
            for (int y = 0; y < mapSizeY; y++)
            {
                //4-way map
                if (x > 0)
                    graph[x, y].edges.Add(graph[x - 1, y]);
                if (x < mapSizeX - 1)
                    graph[x, y].edges.Add(graph[x + 1, y]);
                if (y > 0)
                    graph[x, y].edges.Add(graph[x, y - 1]);
                if (y < mapSizeY - 1)
                    graph[x, y].edges.Add(graph[x, y + 1]);
            }
        }
    }
    

    public Vector3 TileCoordToWorldCoord(int x, int z)
    {
        return new Vector3(x,0,z);
    }

    public bool UnitCanEnterTile(int x, int y)
    {
        return tileTypes[tiles[x,y]].isWalkable;
    }

    public void GeneratePathTo(int x, int y)
    {
        
        unit.GetComponent<Unit>().currentPath = null;

        if(UnitCanEnterTile(x,y) == false)
        {
            return;
        }

        Dictionary<Node, float> dist = new Dictionary<Node, float>();
        Dictionary<Node, Node> prev = new Dictionary<Node, Node>();

        //lista de nodos no checkeados
        List<Node> unvisited = new List<Node>();

        Node source = graph[unit.GetComponent<Unit>().tileX, unit.GetComponent<Unit>().tileY];
       
        Node target = graph[x,y];
        //Starting Pos
        dist[source] = 0;
        prev[source] = null;

        //Initialize inf Distance
        foreach (Node v in graph)
        {
            if(v != source)
            {
                dist[v] = Mathf.Infinity;
                prev[v] = null;
            }
            unvisited.Add(v);
        }
        while(unvisited.Count > 0)
        {
            // u son los casilleros sin visitador
            Node u = null;
            foreach(Node possibleU in unvisited)
            {
                if (u == null|| dist[possibleU] < dist[u])
                {
                    u = possibleU;
                }
            }

            if(u == target)
            {
                break; // sale del loop
            }

            unvisited.Remove(u);

            foreach(Node v in u.edges)
            {
                //float alt = dist[u] + u.DistanceTo(v);
                float alt = dist[u] + CostToEnterTile(u.x, u.y, v.x, v.y);
                if ( alt < dist[v])
                {
                    dist[v] = alt;
                    prev[v] = u;
                }
            }
        }
        if(prev[target] == null)
        {
            //No hay camino entre el comienzo y el fin
            return;
        }
        List<Node> currentPath = new List<Node>();

        Node curr = target;

        while(prev[curr] != null)
        {
            currentPath.Add(curr);
            curr = prev[curr];
        }
        currentPath.Reverse();
        unit.GetComponent<Unit>().currentPath = currentPath;
    }
    
}